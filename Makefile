build:
	GOOS=linux go build main.go
	mv ./main ./pinger

dockerize:
	docker build -t registry.gitlab.com/tmw/kubetest:v3 .

push:
	docker push registry.gitlab.com/tmw/kubetest:v3

clean:
	rm ./pinger
