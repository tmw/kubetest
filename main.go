package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-redis/redis"
	"github.com/satori/go.uuid"
)

type response struct {
	Ping       string `json:"ping"`
	Identifier string `json:"uuid"`
	Count      int64  `json:"count"`
}

func getListeningPort() string {
	if os.Getenv("PORT") != "" {
		return os.Getenv("PORT")
	}
	return "8080"
}

func main() {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	identifier := uuid.Must(uuid.NewV4())
	resp := response{
		Ping:       "Pong!",
		Identifier: identifier.String(),
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("content-type", "application/json")

		// increment Redis counter
		resp.Count = client.Incr("count").Val()

		res, err := json.Marshal(resp)
		if err != nil {
			log.Fatal("Unable to encode response as JSON")
		}

		w.Write(res)
	})

	// start listening
	fmt.Print("Listening on http://localhost:" + getListeningPort())
	http.ListenAndServe(":"+getListeningPort(), mux)
}
